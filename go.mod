module gitlab.com/yagdo/stigmata

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/rpc v1.2.0 // indirect
	github.com/gorilla/schema v1.1.0
)
