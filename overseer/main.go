package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	overseer "gitlab.com/yagdo/stigmata/overseer/overseer"
)

func main() {
	overseer.LoadTemplates()

	serveHTTPAddress := flag.String("address", "127.0.0.1:8000", "The address the http server will listen at")
	reportPath := flag.String("report", "/data/", "the path where the reports are being stored")
	flag.Parse()

	overseer.ReportRootPath = *reportPath

	// DEBUG: INITIALIZE WITH AN AGENT
	overseer.AddAgent("127.0.0.1:8001")

	router := mux.NewRouter()
	router.HandleFunc("/", overseer.ReportsHandler).Methods("GET")
	router.HandleFunc("/new", overseer.ReportOptionsHandler).Methods("GET")
	router.HandleFunc("/new", overseer.CreateNewReportHandler).Methods("POST")
	router.HandleFunc("/agents", overseer.AgentsHandler).Methods("GET")
	router.HandleFunc("/reports/{id}", overseer.ReportHandler).Methods("GET")
	// API routes for service management
	router.HandleFunc("/api/v1/services", overseer.ListServices).Methods("GET")
	router.HandleFunc("/api/v1/services", overseer.LinkService).Methods("POST")
	router.HandleFunc("/api/v1/services/{id}", overseer.UnlinkService).Methods("DELETE")
	// API routes for report management
	router.HandleFunc("/api/v1/reports", overseer.APIReportsHandler).Methods("GET")
	router.HandleFunc("/api/v1/reports/{id}", overseer.APIReportHandler).Methods("GET")
	router.HandleFunc("/api/v1/ages/{id}", overseer.APIAgeReportHandler).Methods("GET")
	router.HandleFunc("/api/v1/options/{id}", overseer.APIOptionsReportHandler).Methods("GET")
	// Static files
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	fmt.Printf("[Overseer]: Service started | Host=http://%s\n", *serveHTTPAddress)
	http.ListenAndServe(*serveHTTPAddress, router)
}
