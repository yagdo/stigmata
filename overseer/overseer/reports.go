package overseer

import (
	"encoding/csv"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/gorilla/mux"
)

// ReportStruct is the json format we are using to indicate a report and it's status
type ReportStruct struct {
	ReportID    string `json:"report_id"`
	IsCompleted bool   `json:"is_completed"`
}

// KeyValue ...
type KeyValue struct {
	Name    string `json:"name"`
	Type    string `json:"type"`
	Value   string `json:"value"`
	Tooltip string `json:"tooltip"`
}

// ReportRootPath ...
var ReportRootPath string

// ListReports returns a list with all the available reports
func ListReports() []ReportStruct {
	var files []ReportStruct
	err := filepath.Walk(ReportRootPath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() && filepath.Base(path) != "data" {
			_, notCompleted := os.Stat(path + "/.completed")
			files = append(
				files,
				ReportStruct{
					ReportID:    filepath.Base(path),
					IsCompleted: notCompleted == nil,
				},
			)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	sort.Slice(files, func(i, j int) bool {
		return files[i].ReportID > files[j].ReportID
	})
	return files
}

// OpenReport opens the report as long as it is available
func OpenReport(reportFileID string) [][]string {
	validFiles := ListReports()
	// Checks if the report is one of the available reports
	// to prevent from looking at other directories
	isValid := false
	for _, _file := range validFiles {
		if _file.IsCompleted && _file.ReportID == reportFileID {
			isValid = true
		}
	}
	if isValid == false {
		return nil
	}

	reportFile, err := os.Open(ReportRootPath + reportFileID + "/data.csv")
	if err != nil {
		panic(err)
	}
	defer reportFile.Close()

	csvr := csv.NewReader(reportFile)
	var fileData [][]string
	for {
		record, err := csvr.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}
		fileData = append(fileData, record)
	}
	return fileData
}

// OpenAges opens the ages.csv as long as it is available
func OpenAges(reportFileID string) [][]string {
	validFiles := ListReports()
	// Checks if the report is one of the available reports
	// to prevent from looking at other directories
	isValid := false
	for _, _file := range validFiles {
		if _file.IsCompleted && _file.ReportID == reportFileID {
			isValid = true
		}
	}
	if isValid == false {
		return nil
	}

	reportFile, err := os.Open(ReportRootPath + reportFileID + "/ages.csv")
	if err != nil {
		panic(err)
	}
	defer reportFile.Close()

	csvr := csv.NewReader(reportFile)
	var fileData [][]string
	for {
		record, err := csvr.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}
		fileData = append(fileData, record)
	}
	return fileData
}

// OpenOptions opens the ages.csv as long as it is available
func OpenOptions(reportFileID string) []byte {
	validFiles := ListReports()
	// Checks if the report is one of the available reports
	// to prevent from looking at other directories
	isValid := false
	for _, _file := range validFiles {
		if _file.IsCompleted && _file.ReportID == reportFileID {
			isValid = true
		}
	}
	if isValid == false {
		return nil
	}

	dat, err := ioutil.ReadFile(ReportRootPath + reportFileID + "/options.json")
	if err != nil {
		panic(err)
	}
	return dat
}

// APIReportsHandler displays the list of reports available in the data directory
// and returns them in a json format
func APIReportsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ListReports())
}

// APIReportHandler displays the list of reports available in the data directory
func APIReportHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fileData := OpenReport(params["id"])
	w.Header().Set("Content-Type", "text/csv")
	json.NewEncoder(w).Encode(fileData)
}

// APIAgeReportHandler displays the list of reports available in the data directory
func APIAgeReportHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fileData := OpenAges(params["id"])
	w.Header().Set("Content-Type", "text/csv")
	json.NewEncoder(w).Encode(fileData)
}

// APIOptionsReportHandler displays the options of the requested report
func APIOptionsReportHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fileData := OpenOptions(params["id"])
	w.Header().Set("Content-Type", "application/json")
	var data interface{}
	json.Unmarshal(fileData, &data)
	json.NewEncoder(w).Encode(data)
}

// ReportsHandler displays the list of reports available in the data directory
func ReportsHandler(w http.ResponseWriter, r *http.Request) {
	var files []ReportStruct = ListReports()
	LoadedTemplates["reports_list.html"].Execute(w, files)
}

// ReportHandler displays a selected report
func ReportHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	LoadedTemplates["report_view.html"].Execute(w, params["id"])
}

// ReportOptionsHandler ...
func ReportOptionsHandler(w http.ResponseWriter, r *http.Request) {
	if len(services) > 0 {
		response, err := http.Get("http://" + services[0].Address + "/options")
		if err != nil {
			return
		}
		var options []KeyValue
		_ = json.NewDecoder(response.Body).Decode(&options)

		LoadedTemplates["report_create.html"].Execute(w, options)
	}

}

func toJSON(m interface{}) string {

	js, err := json.Marshal(m)
	if err != nil {
		log.Fatal(err)
	}

	return strings.ReplaceAll(string(js), ",", ", ")

}

// CreateNewReportHandler ...
func CreateNewReportHandler(w http.ResponseWriter, r *http.Request) {
	if len(services) > 0 {
		r.ParseForm()
		http.PostForm("http://"+services[0].Address+"/run", r.Form)

	}
	ReportOptionsHandler(w, r)

}
