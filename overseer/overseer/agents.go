package overseer

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// LinkedService is a representation of the available services to be used
type LinkedService struct {
	ID      string `json:"id"`
	Address string `json:"address"`
	Status  int    `json:"status"`
	Cycle   int    `json:"cycle"`
}

type statusResponse struct {
	Status int `json:"status"`
	Cycle  int `json:"cycle"`
}

var services []LinkedService
var serviceIdx int = 0

func (agent *LinkedService) checkStatus() {
	response, err := http.Get(agent.Address + "/status")
	if err != nil {
		return
	}
	var status statusResponse
	_ = json.NewDecoder(response.Body).Decode(&status)
	agent.Status = status.Status
	agent.Cycle = status.Cycle

}

// ListServices ...
func ListServices(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	for agentIdx := 0; agentIdx < len(services); agentIdx++ {
		services[agentIdx].checkStatus()
	}
	json.NewEncoder(w).Encode(services)
}

// AddAgent is a fucntion added for debugging purposes.
func AddAgent(address string) {
	services = append(services, LinkedService{ID: strconv.Itoa(rand.Intn(1000000)), Address: address})
}

// LinkService ...
func LinkService(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var service LinkedService
	_ = json.NewDecoder(r.Body).Decode(&service)
	service.ID = strconv.Itoa(rand.Intn(1000000))
	fmt.Printf("[info] registering new service with id:%s @ %s \n", service.ID, service.Address)
	// Before linking check if service exists
	services = append(services, service)
	json.NewEncoder(w).Encode(&service)
}

// UnlinkService ...
func UnlinkService(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range services {
		if item.ID == params["id"] {
			services = append(services[:index], services[index+1:]...)
			break
		}
	}
}

// AgentsHandler displays the list of agents connected
func AgentsHandler(w http.ResponseWriter, r *http.Request) {
	LoadedTemplates["agents_list.html"].Execute(w, services)
}
