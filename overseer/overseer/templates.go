package overseer

import (
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

// TemplatesPath describes the location of the html templates
const TemplatesPath = "templates/"

// LoadedTemplates is a mapping containing the templates of the application
var LoadedTemplates map[string]*template.Template

// LoadTemplates loads every html template available inside the TemplatesPath
func LoadTemplates() {
	LoadedTemplates = make(map[string]*template.Template)
	// USE THIS https://gowebexamples.com/templates/
	err := filepath.Walk(TemplatesPath, func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".html") {
			LoadedTemplates[filepath.Base(path)] = template.Must(template.ParseFiles(path))
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
}
