package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/yagdo/stigmata/pagent/pagent"
)

func main() {

	rand.Seed(time.Now().UnixNano())
	serveHTTP := flag.Bool("serve", false, "If set, this will bind an http server")
	runSimulation := flag.Bool("single-run", false, "If set this will run a single simulation")
	serveHTTPAddress := flag.String("address", "127.0.0.1:3000", "The address the http server will listen at")
	sendHost := flag.Bool("send-host", false, "Register the agent ot the overseer")
	overrideOrigin := flag.String("origin", "", "Set the origin instead of using os.Hostname()")
	reportPath := flag.String("report", "/data/", "the path where the reports are being stored")
	configFile := flag.String("config", "config.json", "Defines the configuration file")

	flag.Parse()

	pagent.InitializeConfiguration(*configFile, *reportPath)
	if *serveHTTP {
		// find hostname because this is going to be wrong
		if *sendHost {
			pagent.RegisterToControl(pagent.Settings.ControlHost, *serveHTTPAddress, *overrideOrigin)
		}

		router := mux.NewRouter()
		router.HandleFunc("/status", pagent.StatusHandler).Methods("GET")
		router.HandleFunc("/options", pagent.OptionsHandler).Methods("GET")

		router.HandleFunc("/run", pagent.StartNewSimulation).Methods("POST")

		fmt.Printf("[pAgent]: Service started | Host=http://%s/status\n", *serveHTTPAddress)
		http.ListenAndServe(*serveHTTPAddress, router)

	}
	if *runSimulation {
		pagent.RunSimulation(pagent.DefaultOptions)
	}

}
