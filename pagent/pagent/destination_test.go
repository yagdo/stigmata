package pagent

import "testing"

var fuzzyDestinationTests int = 100

func TestGenerateHouseWithValidInhabitants(t *testing.T) {
	for iteration := 0; iteration < fuzzyDestinationTests; iteration++ {
		house := GenerateHouse(1000, 1000)
		if house.inhabitants < int(minInhabitants) || house.inhabitants > int(maxInhabitants) {
			t.Errorf("Invalid number of inhabitants in house, got: %d", house.inhabitants)
		}
	}
}
