package pagent

import "testing"

func TestContains(t *testing.T) {
	tables := []struct {
		groupIdx int
		age      int
		isValid  bool
	}{
		{0, 5, true},
		{1, 10, true},
		{2, 20, true},
		{3, 30, true},
		{4, 40, true},
		{5, 50, true},
		{6, 60, true},
		{7, 70, true},
		{8, 80, true},
		{8, 90, true},
		{8, 100, true},

		{2, 5, false},
		{3, 10, false},
		{4, 20, false},
		{5, 30, false},
		{6, 40, false},
		{7, 50, false},
		{8, 60, false},
		{0, 70, false},
		{0, 80, false},
		{1, 90, false},
		{2, 100, false},
	}

	for _, table := range tables {
		inGroup := Covid19MortalityRates[table.groupIdx].contains(table.age)
		if inGroup != table.isValid {
			t.Errorf("Return value mismatch got: %t, want: %t.", inGroup, table.isValid)
		}
	}
}

func TestGetRiskByAge(t *testing.T) {
	tables := []struct {
		entity Entity
		risk   float64
	}{
		{Entity{age: 0}, 0},
		{Entity{age: 5}, 0},
		{Entity{age: 9}, 0},
		{Entity{age: 10}, 0.2},
		{Entity{age: 20}, 0.2},
		{Entity{age: 30}, 0.2},
		{Entity{age: 40}, 0.4},
		{Entity{age: 50}, 1.3},
		{Entity{age: 60}, 3.6},
		{Entity{age: 70}, 8.0},
		{Entity{age: 80}, 14.8},
		{Entity{age: 90}, 14.8},
		{Entity{age: 100}, 14.8},
	}

	for _, table := range tables {
		risk := GetRiskByAge(table.entity)
		if risk != table.risk {
			t.Errorf("Wrong risk. value mismatch got: %f, want: %f.", risk, table.risk)
		}
	}
}
