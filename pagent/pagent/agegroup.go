package pagent

import (
	"log"
	"math"
)

// RiskByAgeGroup represents the mortality risk each age group faces
type RiskByAgeGroup struct {
	lower float64
	upper float64
	risk  float64
}

// Covid19MortalityRates declares the predefined as of now mortality rates
var Covid19MortalityRates = []RiskByAgeGroup{
	{
		lower: 0,
		upper: 9,
		risk:  0,
	},
	{
		lower: 10,
		upper: 19,
		risk:  0.2,
	},
	{
		lower: 20,
		upper: 29,
		risk:  0.2,
	},
	{
		lower: 30,
		upper: 39,
		risk:  0.2,
	},
	{
		lower: 40,
		upper: 49,
		risk:  0.4,
	},
	{
		lower: 50,
		upper: 59,
		risk:  1.3,
	},
	{
		lower: 60,
		upper: 69,
		risk:  3.6,
	},
	{
		lower: 70,
		upper: 79,
		risk:  8.0,
	},
	{
		lower: 80,
		upper: math.Inf(0),
		risk:  14.8,
	},
}

// contains checks if the Risk group contains the age
func (risk_group RiskByAgeGroup) contains(age int) bool {
	return risk_group.lower <= float64(age) && risk_group.upper >= float64(age)
}

// GetRiskByAge returns the risk of an entity
func GetRiskByAge(entity Entity) float64 {
	for _, mortalityGroup := range Covid19MortalityRates {
		if mortalityGroup.contains(entity.age) {
			return mortalityGroup.risk
		}
	}
	log.Fatalf("Age that doesnt belong to a group foun %d", entity.age)
	return .0
}
