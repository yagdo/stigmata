package pagent

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
)

// Universe ...
type Universe struct {
	config              SimulationOptions
	population          []Entity
	lockdownEnforced    bool
	infectedPopulation  int
	deceasedPopulation  int
	recoveredPopulation int
	ageDistribution     map[int]int
}

// EntityActions ...
func (universe *Universe) EntityActions(entity *Entity) {
	if entity.IsEntityInfected() {
		entity.InfectionCycle(universe)
		// we check if the entity is alive
		if !entity.alive {
			return
		}
		// We recheck if the entity is infected to count recoveries

		for target := 0; target < universe.config.Population; target++ {
			if !universe.population[target].alive || universe.population[target].infectionStatus != 0 {
				continue
			}
			if entity.IsClose(universe.population[target]) {
				if universe.population[target].InfectEntity(false) {
					universe.infectedPopulation++
				}

			}
		}

	}
}

// CycleExecution ...
func (universe *Universe) CycleExecution() (bool, []string) {
	var cycleValue = []string{}
	for entity := 0; entity < universe.config.Population; entity++ {
		universe.population[entity].DestinationBasedMovement(*universe)
		universe.EntityActions(&universe.population[entity])

		var entityStatusValue int
		if !universe.population[entity].alive {
			entityStatusValue = 2
		} else if universe.population[entity].IsEntityInfected() {
			entityStatusValue = 1
		} else {
			entityStatusValue = 0
		}
		cycleValue = append(
			cycleValue,
			strconv.Itoa(universe.population[entity].x),
			strconv.Itoa(universe.population[entity].y),
			strconv.Itoa(entityStatusValue),
		)
	}
	if universe.config.LockdownTrigger < float64(universe.infectedPopulation)/float64(universe.config.Population) {
		universe.lockdownEnforced = true
	}
	return universe.infectedPopulation == 0, cycleValue
}

// InitializeUniverse ...
func InitializeUniverse(config SimulationOptions) Universe {
	var housingContainer, businessContainer []Destination
	var populationContainer []Entity

	ageDistribution := make(map[int]int)

	var initialInfectionChance float64 = float64(config.InitialInfectionCases-1) / float64(config.Population)
	var infectedPopulation int = 0

	for home := 0; home < int(config.Population/2000); home++ {
		businessContainer = append(businessContainer, Destination{
			x: rand.Intn(config.XLimit),
			y: rand.Intn(config.YLimit),
		})
	}

	var housedPops int = 0
	for housedPops < config.Population {
		selectedHouse := GenerateHouse(config.XLimit, config.YLimit)
		for inhabitant := 0; inhabitant < selectedHouse.inhabitants; inhabitant++ {

			destinationsRange := rand.Intn(config.MaxAmountOfDestinations-1) + 1
			age := NormalAgeDistribution(config.AgeMean, config.AgeSigma, config.AgeLowerLimit, config.AgeUpperLimit)
			if _, ok := ageDistribution[age]; ok {
				ageDistribution[age]++
			} else {
				ageDistribution[age] = 1
			}

			var selectedDestinations []Destination
			for i := 0; i < destinationsRange; i++ {

				selectedDestinations = append(selectedDestinations, businessContainer[rand.Intn(len(businessContainer))])

			}
			selectedEntity := Entity{
				age:             age,
				x:               selectedHouse.x,
				y:               selectedHouse.y,
				infectionStatus: 0,
				alive:           true,
				home:            &selectedHouse,
				destinations:    &selectedDestinations,
				options:         &config,
			}

			// We infect the first entity and some of the next entities randomly
			if rand.Float64() < initialInfectionChance || housedPops == 0 {
				infectedPopulation++
				selectedEntity.InfectEntity(true)
			}

			populationContainer = append(populationContainer, selectedEntity)
			housedPops++
		}
		housingContainer = append(housingContainer, selectedHouse)

	}
	fmt.Println("[info] Universe initialized successfully")

	return Universe{
		config:              config,
		population:          populationContainer,
		lockdownEnforced:    false,
		infectedPopulation:  infectedPopulation,
		recoveredPopulation: 0,
		deceasedPopulation:  0,
		ageDistribution:     ageDistribution,
	}
}

// SaveAgeDistribution saves the age distribution of the created universe
func (universe Universe) SaveAgeDistribution(ageFile *os.File) {
	for age, occurances := range universe.ageDistribution {
		_, err := ageFile.WriteString(fmt.Sprintf("%d,%d\n", age, occurances))
		if err != nil {
			fmt.Printf("Error writing string: %v", err)
		}
	}
}
