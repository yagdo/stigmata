package pagent

import (
	"math"
	"math/rand"
)

// Entity is the structure which
type Entity struct {
	age             int
	x               int
	y               int
	infectionStatus int
	// Random int between DISEASE_INCUBATION/2 and DISEASE_INCUBATION. If the entity
	// is destined to die then he will die when infectionStatus == markForDeath
	// If the int is 0 then he is destined to recover
	markForDeath int
	alive        bool
	home         *Destination
	destinations *[]Destination
	options      *SimulationOptions
}

const (
	// VULNERABLE ...
	VULNERABLE int = iota
	// INFECTED ...
	INFECTED
	// SYMPTOMATIC ...
	SYMPTOMATIC
	// POSITIVE ...
	POSITIVE
	// RECOVERED ...
	RECOVERED
	// DECEASED ...
	DECEASED
)

// IsClose checks if an entity is within the infection distance
func (entity Entity) IsClose(target Entity) bool {
	first := math.Pow(float64(entity.x-target.x), 2)
	second := math.Pow(float64(entity.y-target.y), 2)
	return math.Sqrt(first+second) < entity.options.InfectionDistance

}

// IsAtHome checks if an entity is at home
func (entity Entity) IsAtHome() bool {
	return entity.x == entity.home.x && entity.y == entity.home.y
}

// IsEntityInfected returns a bool indicating the entity's status
func (entity Entity) IsEntityInfected() bool {
	return entity.infectionStatus > 0 && entity.infectionStatus < entity.options.IncubationDuration

}

// CorrectEntityMovement fixes the entities position if it gets out of bounds
func (entity *Entity) CorrectEntityMovement() {
	if entity.x > entity.options.XLimit {
		entity.x -= entity.options.XLimit
	} else if entity.x < 0 {
		entity.x += entity.options.XLimit
	}
	if entity.y > entity.options.YLimit {
		entity.y -= entity.options.YLimit
	} else if entity.y < 0 {
		entity.y += entity.options.YLimit
	}
}

// DestinationBasedMovement Not integrated yet
func (entity *Entity) DestinationBasedMovement(universe Universe) {
	// TODO Extremelly ill entities shouldn't be able to move
	if entity.IsAtHome() && universe.lockdownEnforced {
		return
	}
	var selectedDestination Destination
	if entity.IsAtHome() {
		selectedDestination = (*entity.destinations)[rand.Intn(len(*entity.destinations))]
	} else {
		selectedDestination = *entity.home
	}
	entity.x = selectedDestination.x
	entity.y = selectedDestination.y
	entity.CorrectEntityMovement()

}

// InfectionCycle ...
func (entity *Entity) InfectionCycle(universe *Universe) {
	entity.infectionStatus++
	if entity.infectionStatus == entity.markForDeath {
		entity.alive = false
		universe.deceasedPopulation++
	}
	if !entity.IsEntityInfected() {
		universe.recoveredPopulation++
		universe.infectedPopulation--
	}
}

// DeceaseEntity ...
func (entity *Entity) DeceaseEntity() bool {
	// random chance of someone dying.
	// https://www.worldometers.info/coronavirus/coronavirus-age-sex-demographics/

	risk := GetRiskByAge(*entity)
	if rand.Float64()*100 < risk {
		return true
	}
	return false
}

// InfectEntity ...
func (entity *Entity) InfectEntity(forced bool) bool {
	// Infection shouldn't be 100%

	if !forced && rand.Float64() > entity.options.InfectionRate {
		return false
	}

	entity.infectionStatus = 1
	if entity.DeceaseEntity() {
		entity.markForDeath = entity.options.IncubationDuration/2 + rand.Intn(entity.options.IncubationDuration/2)
	} else {
		entity.markForDeath = 0
	}
	return true
}
