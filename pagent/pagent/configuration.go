package pagent

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
	"time"
)

// Configuration ...
type Configuration struct {
	ControlHost string
}

// SimulationOptions ...
type SimulationOptions struct {
	IncubationDuration      int
	XLimit                  int
	YLimit                  int
	SimulationCycles        int
	Population              int
	InitialInfectionCases   int
	MaxAmountOfDestinations int
	AgeMean                 float64
	AgeSigma                float64
	AgeLowerLimit           float64
	AgeUpperLimit           float64
	InfectionDistance       float64
	InfectionRate           float64
	LockdownTrigger         float64
}

// KeyValue ...
type KeyValue struct {
	Name    string `json:"name"`
	Type    string `json:"type"`
	Value   string `json:"value"`
	Tooltip string `json:"tooltip"`
}

// Settings is a Configuration which contains all the parameters
// required for the simulation to execute
var Settings Configuration

// DefaultOptions are the options meant to populate the missing options
var DefaultOptions SimulationOptions

var reportRootPath string

// InitializeConfiguration reads a json config file and converts the appropriate
// settings to a Configuration and SimulationOptions
func InitializeConfiguration(filename string, targetReportPath string) {
	reportRootPath = targetReportPath
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		panic(err)
	}
	err = json.NewDecoder(file).Decode(&Settings)
	if err != nil {
		panic(err)
	}
	file.Seek(0, io.SeekStart)
	err = json.NewDecoder(file).Decode(&DefaultOptions)
	if err != nil {
		panic(err)
	}
}

// CreateReportDirectory ...
func CreateReportDirectory() string {
	path := reportRootPath + time.Now().Format("2006_01_02_15_04_05") + "/"
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		panic(err)
	}
	return path
}

// OptionsHandler ...
func getOptionsAsKeys() []KeyValue {
	optionsReflection := reflect.ValueOf(DefaultOptions)
	typeOfOptions := optionsReflection.Type()

	var options []KeyValue

	for option := 0; option < optionsReflection.NumField(); option++ {
		if tooltipValue, ok := OptionsTooltipHelper[typeOfOptions.Field(option).Name]; ok {
			options = append(
				options,
				KeyValue{
					Name:    typeOfOptions.Field(option).Name,
					Type:    typeOfOptions.Field(option).Type.String(),
					Value:   fmt.Sprintf("%v", optionsReflection.Field(option).Interface()),
					Tooltip: tooltipValue,
				},
			)
		} else {
			options = append(
				options,
				KeyValue{
					Name:    typeOfOptions.Field(option).Name,
					Type:    typeOfOptions.Field(option).Type.String(),
					Value:   fmt.Sprintf("%v", optionsReflection.Field(option).Interface()),
					Tooltip: "",
				},
			)
		}

	}
	return options
}
