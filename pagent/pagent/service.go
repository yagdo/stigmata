package pagent

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/schema"
)

const (
	// IDLE means the service is currently idle
	IDLE int = iota
	// RUNNING  means the service is currently running
	RUNNING
)

// ServiceResponse ...
type ServiceResponse struct {
	Status int    `json:"status"`
	Cycle  int    `json:"cycle"`
	Report string `json:"report"`
}

var currentStatus int = IDLE
var currentCycle int = 0
var latestReportPath string = ""

// RunSimulation ...
func RunSimulation(config SimulationOptions) {
	latestReportPath = CreateReportDirectory()
	currentStatus = RUNNING

	dataFile, err := os.Create(latestReportPath + "data.csv")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer dataFile.Close()

	ageFile, err := os.Create(latestReportPath + "ages.csv")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer ageFile.Close()

	universe := InitializeUniverse(config)

	ageWriter := csv.NewWriter(ageFile)
	defer ageWriter.Flush()
	universe.SaveAgeDistribution(ageFile)

	writer := csv.NewWriter(dataFile)
	defer writer.Flush()

	for cycle := 0; cycle < universe.config.SimulationCycles; cycle++ {
		finalCycle, cycleValue := universe.CycleExecution()
		fmt.Printf("[info] Cycle #%d\n", cycle)
		currentCycle = int((float64(cycle) / float64(universe.config.SimulationCycles)) * 100)

		err := writer.Write(cycleValue)
		if err != nil {
			log.Fatalf("Failed writing to file: %s", err)
		}
		if finalCycle {
			break
		}

	}
	currentStatus = IDLE
	currentCycle = 0

	optionsAsJSON, _ := json.MarshalIndent(config, "", " ")
	_ = ioutil.WriteFile(latestReportPath+"options.json", optionsAsJSON, 0644)

	completeFile, err := os.Create(latestReportPath + ".completed")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	defer completeFile.Close()

}

// RegisterToControl ...
func RegisterToControl(targetHost string, listeningHost string, origin string) {
	var address string = origin
	if origin == "" {
		hostname, err := os.Hostname()
		if err != nil {
			panic(err)
		}
		listeningHostPort := strings.Split(listeningHost, ":")[1]
		address = "http://" + hostname + ":" + listeningHostPort
	}
	values := map[string]interface{}{
		"address": address,
		"status":  currentStatus,
		"cycle":   currentCycle,
	}
	jsonValue, _ := json.Marshal(values)
	_, err := http.Post("http://"+targetHost+"/api/v1/services", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		panic(err)
	}
	fmt.Printf("[info] registered @ %s with address %s\n", targetHost, values["address"])
}

// StatusHandler returns the agent's status in the following form
// {
// 	"status": 0,           # the status of the agent 0 if IDLE and 1 if RUNNING
// 	"cycle" : 0,           # the current cycle of the running simulation
// 	"report": "/data/...", # the latest report created
// }
func StatusHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	f := ServiceResponse{
		Status: currentStatus,
		Cycle:  currentCycle,
		Report: latestReportPath,
	}
	json.NewEncoder(w).Encode(&f)

}

// OptionsHandler returns the available options
func OptionsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var options []KeyValue = getOptionsAsKeys()
	json.NewEncoder(w).Encode(&options)
}

// StartNewSimulation ...
func StartNewSimulation(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	options := new(SimulationOptions)
	decoder := schema.NewDecoder()
	decoder.Decode(options, r.Form)
	if currentStatus == IDLE {
		go RunSimulation(*options)
	}
	StatusHandler(w, r)

}
