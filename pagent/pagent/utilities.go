package pagent

import (
	"math/rand"
)

// NormalAgeDistribution generates ages between GlobalAgeLowerLimit and upperLimit
// which follow a normal distribution using a defined mean and sigma value
func NormalAgeDistribution(mean, sigma, lowerLimit, upperLimit float64) int {
	age := float64(mean) + rand.NormFloat64()*float64(sigma)
	if age < lowerLimit || age > upperLimit {
		return NormalAgeDistribution(mean, sigma, lowerLimit, upperLimit)
	}
	return int(age)
}
