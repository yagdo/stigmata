package pagent

// OptionsTooltipHelper is the localization map which holds the tooltips which will
// display over the UI
var OptionsTooltipHelper = map[string]string{
	"IncubationDuration":      "The period as days required for the disease to incubate",
	"XLimit":                  "The x-axis limit of the space defined",
	"YLimit":                  "The y-axis limit of the space defined",
	"SimulationCycles":        "Max cycles the simulation is going to run for",
	"Population":              "Amount of entities the universe is initialized with",
	"InitialInfectionCases":   "Initial infected entities",
	"MaxAmountOfDestinations": "Max amount of possible destinations an entity can choose",
	"AgeMean":                 "The mean age of a population",
	"AgeSigma":                "The sigma of a population's age",
	"AgeLowerLimit":           "Lowest possible age",
	"AgeUpperLimit":           "Highest possible age",
	"InfectionDistance":       "Distance within an infection is possible",
	"InfectionRate":           "Chance of an infection happening between two enties within the infection distance",
	"LockdownTrigger":         "Triggers a lockdown when the infected population crosses the threshold",
}
