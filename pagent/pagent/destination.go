package pagent

import "math/rand"

const minInhabitants float64 = 1.0
const maxInhabitants float64 = 6.0
const avgInhabitants float64 = 2.0

// Destination will be a list of destinations designated for the entity.
// The entity will be moving between his destinations while there is no lockdown
type Destination struct {
	x           int
	y           int
	inhabitants int
	isCritical  bool
}

// GenerateHouse generates houses with inhabitants
// which follow a normal distribution using a defined mean and sigma value1
// TODO use a more accurate configuration
func GenerateHouse(xLimit, yLimit int) Destination {
	inhabitants := avgInhabitants + rand.NormFloat64()
	if inhabitants < minInhabitants || inhabitants > maxInhabitants {
		return GenerateHouse(xLimit, yLimit)
	}
	return Destination{
		x:           rand.Intn(xLimit),
		y:           rand.Intn(yLimit),
		isCritical:  false,
		inhabitants: int(inhabitants),
	}

}
