var _BLUE = '#36a2eb'
var _PURPLE = '#cc65fe'
var _RED = '#ff6384'
var _LABELS = ['Healthy', 'Infected', 'Deceased']
var populationContext = document.getElementById('populationChart').getContext('2d');
var populationChart = new Chart(populationContext, {
    type: 'scatter',
    data: {
        labels: _LABELS,

        datasets: [
          {
            label: 'Healthy',
            data: [],
            order: 2,
            pointBackgroundColor: _BLUE,
            backgroundColor: _BLUE,
          },
          {
            label: 'Inflicted',
            data: [],
            order: 1,
            pointBackgroundColor: _PURPLE,
            backgroundColor: _PURPLE,
          },
          {
            label: 'Deceased',
            data: [],
            order: 0,
            pointBackgroundColor: _RED,
            backgroundColor: _RED,
          },
        ]
    },
    options: {
        scales: {
            yAxes: [{
              ticks: {
                suggestedMin: 0,
                suggestedMax: 10000,
              }
            }],

            xAxes: [{
              ticks: {
                suggestedMin: 0,
                suggestedMax: 10000,
              }
            }]
        },
        animation: {
            duration: 0 /* general animation time */
        },
        hover: {
            animationDuration: 0 /* duration of animations when hovering an item */
        },
    }
});


function ScatterChartUpdate(_, row) {
    populationChart.data.datasets[0].data = row[0]
    populationChart.data.datasets[1].data = row[1]
    populationChart.data.datasets[2].data = row[2]
    populationChart.update();
}