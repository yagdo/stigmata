var _BLUE = '#36a2eb'
var _PURPLE = '#cc65fe'
var _RED = '#ff6384'
var _LABELS = ['Healthy', 'Infected', 'Deceased']

var ageContext = document.getElementById('ageChart').getContext('2d');
var ageChart = new Chart(ageContext, {
    type: 'scatter',
    data: {
      datasets: [
          {
              label: 'Population',
              data: [],
              fill: false,
              backgroundColor: _BLUE,
          },
      ]
  },
    options: {
      responsive: true,
      maintainAspectRatio: false
    }
});

