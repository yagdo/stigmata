var _BLUE = '#36a2eb'
var _PURPLE = '#cc65fe'
var _RED = '#ff6384'
var _LABELS = ['Healthy', 'Infected', 'Deceased']

var SIRContext = document.getElementById('SIRChart').getContext('2d');
var SIRChart = new Chart(SIRContext, {
    type: 'line',
    data: {
        datasets: [
            {
                label: 'Healthy',
                data: [],
                fill: false,
                borderColor: _BLUE,
            },
            {
                label: 'Inflicted',
                data: [],
                fill: false,
                borderColor: _PURPLE,
            },
            {
                label: 'Deceased',
                data: [],
                fill: false,
                borderColor: _RED,
            },
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false
    }
});

function SIRChartUpdate(frame, row) {
    if (frame < SIRChart.data.datasets[0].data.length) {
        SIRChart.data.datasets[0].data.pop()
        SIRChart.data.datasets[1].data.pop()
        SIRChart.data.datasets[2].data.pop()
    } else  {
        SIRChart.data.datasets[0].data.push(row[0].length)
        SIRChart.data.datasets[1].data.push(row[1].length)
        SIRChart.data.datasets[2].data.push(row[2].length)
    }
    SIRChart.update();
}