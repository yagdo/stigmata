var playerID = null;
var frameCount;
var currentFrame = 0;
var interval = parseInt($('#playerInterval').val());
var registeredCharts = [];

function StartPlayer() {
    if (playerID == null) {
        interval = parseInt($('#playerInterval').val());
        playerID = setInterval(RenderCharts, interval)
    }
}

function PausePlayer() {
    clearInterval(playerID);
    playerID = null;
}

function ResetPlayer() {
    currentFrame = 0;
    RenderCharts();
}

function OneFrameBack() {
    currentFrame = currentFrame - 2;
    if (currentFrame < 0) {
        currentFrame = currentFrame + frameCount;
    }
    RenderCharts();
}

function OneFrameForward() {
    currentFrame = currentFrame + 1;
    if (currentFrame >= frameCount) {
        currentFrame = currentFrame - frameCount;
    }
    RenderCharts();

}

function organize_row(row) {
    var datasets = {
      0: [], 
      1: [], 
      2: []
    }
    for (element = 0; element < row.length; element += 3) {
      datasets[row[element+2]].push({x: row[element+0], y: row[element+1]})
    }

    return datasets
  }

function RenderCharts() {
    row = organize_row(data[currentFrame]);
    registeredCharts.forEach(registeredChart => {
        registeredChart(currentFrame, row);
    });
    currentFrame += 1; // TODO this creates issues
    $('#frame').progress({percent: (currentFrame/frameCount)*100})
    if (currentFrame >= frameCount) {
        clearInterval(playerID);
        playerID = null;
        currentFrame = 0;
    }
}

function EnableControls() {
    $("#sBackwardButton").removeClass('disabled');
    $("#sBackwardButton").on("click", OneFrameBack);
    $("#resetButton").removeClass('disabled');
    $("#resetButton").on("click", ResetPlayer);
    $("#playButton").removeClass('disabled');
    $("#playButton").on("click", StartPlayer);
    $("#pauseButton").removeClass('disabled');
    $("#pauseButton").on("click", PausePlayer);
    $("#sForwardButton").removeClass('disabled');
    $("#sForwardButton").on("click", OneFrameForward);
}