Stigmata
========

Stigmata is a project inspired from [paulvangentcom's project](https://github.com/paulvangentcom/python_corona_simulation), written in golang so I can become proficient with the language. More specifically it is a set of microservices which coordinate and run coronavirus simulations.

Services included in the architecture:
* overseer service
* pAgent service
* contacts service (TBImplemented)


### Deployment

Copy the `config.json.example` to `config.json` as it will be used as the default 
configuration of the services. Change any variables as you see fitting.

You can then use docker-compose to deploy the microservices with the following command:
`sudo docker-compose up --build -d`

The Web administration UI should be available on [http://127.0.0.1:8080](http://127.0.0.1:8080)

### Testing 

You can test the modules using the following make command `make test`