# Greatly inspired by following gist:
# https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db
TAG := $(shell git log -1 --pretty=format:"%h")

APP_NAME = "Stigmata"

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
# .PHONY: help
.PHONY: clean clean-pyc help
.DEFAULT_GOAL := help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

run-overseer: ## Creates an overseer service
	mkdir -p data
	go run overseer/main.go -address=:8000 -report=data/

run-pagent: ## Creates a pagent service
	mkdir -p data
	go run pagent/main.go -serve -send-host -address=127.0.0.1:8001 -report=data/ -origin=127.0.0.1:8001

test: ## Executes the unittests
	cd pagent/pagent && go test
	cd overseer/overseer && go test

